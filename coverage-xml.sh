#!/bin/bash

cd build/coverage

./osef-gtest "$@"

gcovr . --exclude-directories CMakeFiles --exclude-directories googletest-build -r ../../.. --xml-pretty -o osef-gtest.xml
