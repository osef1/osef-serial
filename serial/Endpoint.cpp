#include "Endpoint.h"
#include "Debug.h"

bool OSEF::Endpoint::sendString(const std::string& s)
{
    bool ret = false;

    if (connectNetwork())
    {
        ret = sendToNet(s);
    }

    return ret;
}

bool OSEF::Endpoint::receiveString(std::string& s, const size_t& size)
{
    bool ret = false;

    if (connectNetwork())
    {
        ret = receiveFromNet(s, size);
    }

    return ret;
}

bool OSEF::Endpoint::receiveString(std::string& s, const size_t& size, const timespec& to)
{
    bool ret = false;

    if (connectNetwork())
    {
        ret = receiveFromNet(s, size, to);
    }

    return ret;
}
