#include "SerialEndpoint.h"
#include "Debug.h"

#include <fcntl.h>  // O_RDWR
#include <unistd.h>  // close

OSEF::SerialEndpoint::SerialEndpoint(const std::string& nm, const speed_t& spd)
    :name(nm),
    serialFd(-1),
    speed(spd) {}

OSEF::SerialEndpoint::~SerialEndpoint()
{
    closeSerial();
}

void OSEF::SerialEndpoint::closeSerial()
{
    if (serialFd > 0)
    {
        close(serialFd);
        serialFd = -1;
    }
}

void OSEF::SerialEndpoint::displayControlFlags(const struct termios& options)
{
    std::string flags;

    switch (options.c_cflag & CSIZE)
    {
        case CS5:
            flags += " CS5";
            break;
        case CS6:
            flags += " CS6";
            break;
        case CS7:
            flags += " CS7";
            break;
        case CS8:
            flags += " CS8";
            break;
        default:
            break;
    }

    if ((options.c_cflag & CSTOPB) != 0U)
    {
        flags += " CSTOPB";
    }

    if ((options.c_cflag & CREAD) != 0U)
    {
        flags += " CREAD";
    }

    if ((options.c_cflag & PARENB) != 0U)
    {
        flags += " PARENB";
    }

    if ((options.c_cflag & PARODD) != 0U)
    {
        flags += " PARODD";
    }

    if ((options.c_cflag & HUPCL) != 0U)
    {
        flags += " HUPCL";
    }

    if ((options.c_cflag & CLOCAL) != 0U)
    {
        flags += " CLOCAL";
    }

    std::cout << "Serial control flags " << flags << std::endl;
}

void OSEF::SerialEndpoint::displayBaudrate(const struct termios& options)
{
    std::string br = "asymetric ...";

    if (options.c_ispeed == options.c_ospeed)
    {
        switch (options.c_ispeed)
        {
            case B9600:
                br = std::to_string(9600);
                break;
            case B115200:
                br = std::to_string(115200);
                break;
            default:
                br = "enum " + std::to_string(options.c_ispeed);
                break;
        }
    }

    std::cout << "Serial baud rate " << br << std::endl;
}

void OSEF::SerialEndpoint::customizeOptions(struct termios& options)
{
    // Control modes
    options.c_cflag &= ~CSIZE;  // no byte size
    options.c_cflag |= CS8;  // 8 bits byte size
    options.c_cflag &= ~PARENB;  // no parity
    options.c_cflag &= ~CSTOPB;  // 1 stop bit

    options.c_cflag |= CLOCAL;  // ignore control lines
    options.c_cflag &= ~CRTSCTS;  // disable hardware flow control
    options.c_cflag &= ~HUPCL;  // disable lower control lines after close

    options.c_cflag |= CREAD;  // turn on READ

    // Local modes
    options.c_lflag &= ~ECHO;  // disable echo
    options.c_lflag &= ~ECHOE;  // disable echo erase character as error-correcting backspace
    options.c_lflag &= ~ECHOK;  // disable echo kill
    options.c_lflag &= ~ECHONL;  // disable echo new-line
    options.c_lflag &= ~ICANON;  // disable canonical input mode
    options.c_lflag &= ~IEXTEN;  // disable extended input character processing
    options.c_lflag &= ~ISIG;  // disable signals (INTR, QUIT and SUSP)

    // Input modes
    options.c_iflag &= ~(IXON | IXOFF | IXANY);  // Turn off software flow control
    options.c_iflag &= ~(IGNBRK|BRKINT|PARMRK|ISTRIP|INLCR|IGNCR|ICRNL);  // Disable any special handling of received bytes

    // Output modes
    options.c_oflag &= ~OPOST;  // Prevent special interpretation of output bytes (e.g. newline chars)
    options.c_oflag &= ~ONLCR;  // Prevent conversion of newline to carriage return/line feed

    // polling read
    options.c_cc[VTIME] = 0;
    options.c_cc[VMIN] = 0;
}

bool OSEF::SerialEndpoint::connectNetwork()
{
    bool ret = false;

    if (serialFd <= 0)
    {
        serialFd = open(name.c_str(), O_RDWR | O_NOCTTY | O_CLOEXEC);
        if (serialFd > 0)
        {
            // Open the device in nonblocking mode
            struct termios options{};
            if (tcgetattr(serialFd, &options) == 0)
            {
                customizeOptions(options);

                // Configure baud rate
                if (cfsetspeed(&options, speed) == 0)
                {
                    // Set configuration
                    if (tcsetattr(serialFd, TCSANOW, &options) == 0)
                    {
                        DOUT("Serial endpoint " << name << " opened with 8 bits bytes, no parity, one stop bit and no flow control");
#ifdef DEBUG
                        displayControlFlags(options);
                        displayBaudrate(options);
#endif
                        ret = true;
                    }
                    else
                    {
                        DERR("error setting serial device configuration");
                        closeSerial();
                    }
                }
                else
                {
                    DERR("error configuring baud rate");
                    closeSerial();
                }
            }
            else
            {
                DERR("error getting serial file descriptor options");
                closeSerial();
            }
        }
        else
        {
            DERR("error opening serial file descriptor");
        }
    }
    else
    {
        ret = true;
    }

    return ret;
}

bool OSEF::SerialEndpoint::sendToNet(const std::string& s)
{
    bool ret = false;

    const ssize_t sendL = write(serialFd, s.c_str(), s.size());

    // negative sendL becomes positive after size_t cast
    // hence >= 0 test is necessary
    if ((sendL >= static_cast<ssize_t>(0))
        && (static_cast<size_t>(sendL) == s.size()))
    {
        DOUT("sent " << sendL << " bytes");
        ret = true;
    }

    if (not ret)
    {
        DWARN("error sending to serial port " << name << " errno " << errno << " " << strerror(errno));
        close(serialFd);
    }

    return ret;
}

bool OSEF::SerialEndpoint::receiveBytes(std::string& s, const size_t& size) const
{
    bool ret = false;

    char c = 0;
    ssize_t rr = 1;

    s = "";

    while ( (s.size() < size)
            && (rr == 1))
    {
        rr = read(serialFd, &c, 1);
        if (rr == 1)
        {
            s.append(1, c);
        }
    };

    if (rr == 0)
    {
        DOUT("received " << s.size() << " bytes");
        ret = true;
    }
    else
    {
        DWARN("error receiving from serial port " << name << " errno " << errno << " " << strerror(errno));
    }

    return ret;
}

bool OSEF::SerialEndpoint::receiveFromSerial(std::string& s, const size_t& size, const timespec* to)
{
    bool ret = false;

    if (size > 0)
    {
        fd_set readfds;
        FD_ZERO(&readfds);
        FD_SET(serialFd, &readfds);

        const int32_t ps = pselect(serialFd + 1, &readfds, nullptr, nullptr, to, nullptr);
        if (ps == 1)
        {
            ret = receiveBytes(s, size);
        }
        else
        {
            if (ps != 0)
            {
                DERR("select read error returned " << ps);
            }
        }
    }
    else
    {
        DWARN("buffer size null");
    }

    return ret;
}

bool OSEF::SerialEndpoint::receiveFromNet(std::string& s, const size_t& size)
{
    return receiveFromSerial( s, size, nullptr);
}

bool OSEF::SerialEndpoint::receiveFromNet(std::string& s, const size_t& size, const timespec& to)
{
    return receiveFromSerial( s, size, &to);
}
