#ifndef OSEFENDPOINT_H
#define OSEFENDPOINT_H

#include <string>

namespace OSEF
{
    class Endpoint
    {
    public:
        Endpoint() = default;
        virtual ~Endpoint() = default;

        bool sendString(const std::string& s);
        bool receiveString(std::string& s, const size_t& size);
        bool receiveString(std::string& s, const size_t& size, const timespec& to);

        Endpoint(const Endpoint&) = delete;  // copy constructor
        Endpoint& operator=(const Endpoint&) = delete;  // copy assignment
        Endpoint(Endpoint&&) = delete;  // move constructor
        Endpoint& operator=(Endpoint&&) = delete;  // move assignment

    private:
        virtual bool connectNetwork() = 0;

        virtual bool sendToNet(const std::string& s) = 0;
        virtual bool receiveFromNet(std::string& s, const size_t& size) = 0;
        virtual bool receiveFromNet(std::string& s, const size_t& size, const timespec& to) = 0;
    };
}  // namespace OSEF

#endif /* OSEFENDPOINT_H */
