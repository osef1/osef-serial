#ifndef OSEFSERIALENDPOINT_H
#define OSEFSERIALENDPOINT_H

#include <termios.h>  // speed_t
#include <string>

#include "Endpoint.h"

namespace OSEF
{
    class SerialEndpoint : public Endpoint
    {
    public:
        explicit SerialEndpoint(const std::string& nm, const speed_t& spd = B115200);  ///< server constructor with reception socket
        ~SerialEndpoint() override;

        SerialEndpoint(const SerialEndpoint&) = delete;  // copy constructor
        SerialEndpoint& operator=(const SerialEndpoint&) = delete;  // copy assignment
        SerialEndpoint(SerialEndpoint&&) = delete;  // move constructor
        SerialEndpoint& operator=(SerialEndpoint&&) = delete;  // move assignment

    private:
        bool connectNetwork() override;
        void customizeOptions(struct termios& options);

        void displayControlFlags(const struct termios& options);
        void displayBaudrate(const struct termios& options);

        bool sendToNet(const std::string& s) override;
        bool receiveFromNet(std::string& s, const size_t& size) override;
        bool receiveFromNet(std::string& s, const size_t& size, const timespec& to) override;

        bool receiveFromSerial(std::string& s, const size_t& size, const timespec* to);
        bool receiveBytes(std::string& s, const size_t& size) const;

        void closeSerial();

        std::string name;
        int32_t serialFd;
        speed_t speed;
    };
}  // namespace OSEF

#endif /* OSEFSERIALENDPOINT_H */
