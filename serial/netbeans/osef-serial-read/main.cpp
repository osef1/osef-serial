#include "SerialEndpoint.h"

#include <iostream>

int main()
{
    int ret = -1;

    const std::string port = "/dev/ttyS0";
    OSEF::SerialEndpoint serial(port);

    std::string msg;
    if (serial.receiveString(msg, 128))
    {
        std::cout << "received " << msg << " on serial port " << port << std::endl;
        ret = 0;
    }

    return ret;
}
