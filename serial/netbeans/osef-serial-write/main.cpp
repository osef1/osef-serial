#include "SerialEndpoint.h"

#include <iostream>

int main()
{
    int ret = -1;

    const std::string port = "/dev/ttyS0";
    OSEF::SerialEndpoint serial(port);

    std::string msg = "abc";
    if (serial.sendString(msg))
    {
        std::cout << "sent " << msg << " on serial port " << port << std::endl;
        ret = 0;
    }

    return ret;
}
