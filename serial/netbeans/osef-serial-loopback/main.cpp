#include "SerialEndpoint.h"

#include <iostream>

int main()
{
    int ret = -1;

    std::string port = "/dev/ttyS0";
    OSEF::SerialEndpoint endpoint(port);
    std::string msg = "abc";
    if (endpoint.sendString(msg))
    {
        std::cout << "sent " << msg << " on serial port " << port << std::endl;

        msg = "";
        if (endpoint.receiveString(msg, 128))
        {
            std::cout << "received " << msg << " on serial port " << port << std::endl;
            ret = 0;
        }
    }

    return ret;
}
